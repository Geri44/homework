<script>
const input = document.getElementById("inputtext");
input.addEventListener("focus", function () {
    this.style.outline = "2px solid green";
    const span = document.querySelector(".newSpan")
    if (span) {
        span.remove()
    }
    const correctPrice = document.querySelector(".divcorrectPrice");
    if (correctPrice) {
        correctPrice.remove()
    }
})
input.addEventListener("blur", function () {
    if (input.value <= 0) {
        this.style.outline = "solid 2px red";
        this.style.background = "white";;
        const correctPrice = document.createElement("div");
        correctPrice.classList.add("divcorrectPrice");
        correctPrice.innerHTML = `Please enter correct price`;
        input.after(correctPrice);
    } else {
        this.style.outline = "none";
        input.classList.add("input-background");

        const span = document.createElement("span");
        span.classList.add("newSpan");
        span.innerHTML = `Текущая цена: ${input.value} <span class="close">x</span>`;
        input.before(span);
        span.addEventListener("click", function(e){
            if (e.target.classList.contains("close")) {
                e.target.parentElement.remove();
            }
        })
        }
    })
    </script>