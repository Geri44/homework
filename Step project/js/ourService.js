const tabsTitle = document.querySelectorAll(".tabs__caption li");
for (let i = 0; i < tabsTitle.length; i++) {
    tabsTitle[i].addEventListener('click', function () {

        const parentUl = this.closest(".tabs__caption");
        for (let i = 0; i < parentUl.children.length; i++) {
            parentUl.children[i].classList.remove("active");
        }

        this.classList.add("active");

        const tabsContent = document.querySelectorAll(".tabs .tabs__content");
        for (let i = 0; i < tabsContent.length; i++) {
            tabsContent[i].classList.remove("active");
        }

        const tabId = this.dataset.target;
            //const tabId = this.getAttribute("data-target");
        console.log(tabId);

        const tab = document.getElementById(tabId);
        tab.classList.add("active");
    });
}


