$(document).ready(function() {

    $('.menu li a').click(function(event) {

        event.preventDefault();

        $('.menu li').removeClass('selected');
        $(this).parent('li').addClass('selected');

        let imgWidth = '278px';
        let imgHeight = '200px'
        let thisItem 	= $(this).attr('rel');

        if(thisItem !== "all") {

            $('.item li[rel='+thisItem+']').stop()
                .animate({'width' : imgWidth,
                    'height' : imgHeight,
                    'opacity' : 1,
                    'marginRight' : 0,
                    'marginLeft' : 0

                });


            $('.item li[rel!='+thisItem+']').stop()
                .animate({'width' : 0,
                    'height' : imgHeight,
                    'opacity' : 1,
                    'marginRight' : 0,
                    'marginLeft' : 0

                });
        } else {

            $('.item li').stop()
                .animate({'opacity' : 1,
                    'height' : imgHeight,
                    'marginRight' : 0,
                    'marginLeft' : 0,
                    'width' : imgWidth,
                });
        }
    })

    $('.item li img').animate({'opacity' : 1}).hover(function() {
        $(this).animate({'opacity' : 1});
    }, function() {
        $(this).animate({'opacity' : 1});
    });

});



$('.classForSlick').slick({
    dots: false,
    arrows: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    adaptiveHeight: true
});
$('.classForSlick2').slick({
    dots: false,
    centerMode: true,
    arrows:true,
    prevArrow: '.myNawLeft',
    nextArrow: '.myNawRight',
    infinite: true,
    speed: 300,
    slidesToScroll: 1,
    // slidesToShow: 4,
    adaptiveHeight: true,
    asNavFor:'.classForSlick',
    focusOnSelect: true,
    variableHeight: true,
    variableWidth: true
});


